{
    'name': 'POS test',
    'version': '1.0.0',
    'category': 'web',
    'sequence': 3,
    'author': 'hozz',
    'depends': ['web','point_of_sale','mail'],
    'data': [
        # 'change_view.xml',
        'templates.xml',

    ],
    'qweb':['static/src/xml/poschange.xml'],
    'installable': True,
    'application': True,
    'auto_install': False,
}